import { FC } from 'react';
import useSceneTransition from '../../hooks/useSceneTransition';
import useBuildStore from '../../store/useBuildStore';
import './index.scss';

export interface SceneTransitionProps {
  toggleMapValue: boolean;
  transitionDiv: React.RefObject<HTMLDivElement>;
  startTransitionBetweenScenes: () => void;
}

const SceneTransition: FC<SceneTransitionProps> = ({ toggleMapValue, transitionDiv, startTransitionBetweenScenes }) => {
  const selectedBuilding = useBuildStore((store) => store.selectedBuilding)
  const loadingProgress = useBuildStore((state) => state.loadingProgress);
  useSceneTransition({
    toggleMapValue,
    transitionDiv,
    startTransitionBetweenScenes
  })

  return (
    <div className='scene-transition' ref={transitionDiv}>
      <img src={`./logos/${selectedBuilding}.svg`} className='logo' />
      <div className='loading'>
        LOADING
        <div className='progress' style={{ width: `${loadingProgress}%` }}></div>
      </div>
    </div>
  )
}

export default SceneTransition;
