import { FC } from 'react';
import { useProgress } from "@react-three/drei";
import useExperienceStore from '../../store/useExperienceStore';
import './index.scss'

interface LoadingScreenProps {
  setShowLoadingScreen: React.Dispatch<React.SetStateAction<boolean>>;
}

const LoadingScreen:FC<LoadingScreenProps> = ({ setShowLoadingScreen }) => {
  const { progress } = useProgress();
  const isExperienceStarted = useExperienceStore((store) => store.isExperienceStarted)
  const startExperience = useExperienceStore((store) => store.startExperience)

  return (
    <div className='loading-screen' style={{ top: isExperienceStarted ? '-100%' : 0 }}>
      <div className='video-wrapper'>
        <video autoPlay muted loop>
          <source src="./video-bg.mp4" type="video/mp4" />
        </video>
      </div>

      <div className='intro'>
        <div className='logo'>EVOLUTIONS</div>
        <h1>Dubai’s First Real Estate <br /> Intelligence Hub</h1>
        <div className={'wrapper' + (progress === 100 ? ' swipe' : '')}>
          <div className='loading'>
            <p>Loading experience</p>
            <div className='loading-progress' style={{ transform: `translate(${progress - 100}%, 0)` }}></div>
          </div>
          <button
            className='default'
            onClick={() => {
              startExperience()
              setTimeout(() => setShowLoadingScreen(false), 800)
            }}
          >
            Start
          </button>
        </div>
      </div>
    </div>
  )
}

export default LoadingScreen;
