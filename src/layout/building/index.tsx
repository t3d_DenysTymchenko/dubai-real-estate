import { FC, useState } from 'react';
import BuildingPreview from '../../ui/building/preview';
import BuildingPanel from '../../ui/building/panel';
import BuildingInfo from '../../ui/building/info';
import BuildingGallery from '../../ui/building/gallery';

interface BuildingLayoutProps {
  changeScene: () => void;
};

const BuildingLayout: FC<BuildingLayoutProps> = ({ changeScene }) => {
  const [showGallery, setShowGallery] = useState(false);
  const [toggleInfo, setToggleInfo] = useState(true);

  return (
    <>
      <BuildingPanel changeScene={changeScene} setShowGallery={setShowGallery} setToggleInfo={setToggleInfo} />
      <BuildingPreview />
      {showGallery && <BuildingGallery setShowGallery={setShowGallery} />}
      <BuildingInfo toggleInfo={toggleInfo} setToggleInfo={setToggleInfo} />
    </>
  );
}

export default BuildingLayout;
