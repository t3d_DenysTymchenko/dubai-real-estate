import { create } from 'zustand'

interface buildState {
  selectedBuilding: string;
  setSelectedBuilding: (building: string) => void;
  loadingProgress: number;
  setLoadingProgress: (updatedLoadingProgress: number) => void;
}

const useBuildStore = create<buildState>()((set) => ({
  selectedBuilding: new URL(window.location.href).searchParams.get('build') || '',
  setSelectedBuilding: (building: string) => set(() => ({ selectedBuilding: building.toLowerCase() })),
  loadingProgress: 0,
  setLoadingProgress: (updatedLoadingProgress: number) => set(() => ({ loadingProgress: updatedLoadingProgress }))
}))

export default useBuildStore;
