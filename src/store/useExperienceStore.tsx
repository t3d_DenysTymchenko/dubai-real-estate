import { create } from 'zustand'

interface experienceState {
  isExperienceStarted: boolean;
  startExperience: () => void;
  startTransitionEnded: boolean;
  endStartTransition: () => void;
  showMap: boolean;
  toggleShowMap: () => void;
  showHint: boolean;
  toggleShowHint: () => void;
}

const useExperienceStore = create<experienceState>()((set) => ({
  isExperienceStarted: false,
  startExperience: () => set(() => ({ isExperienceStarted: true })),

  startTransitionEnded: false,
  endStartTransition: () => set(() => ({ startTransitionEnded: true })),

  showMap: !Boolean(new URL(window.location.href).searchParams.get('build')),
  toggleShowMap: () => set((state) => ({ showMap: !state.showMap })),

  showHint: false,
  toggleShowHint: () => set((state) => ({ showHint: !state.showHint }))
}))

export default useExperienceStore;
