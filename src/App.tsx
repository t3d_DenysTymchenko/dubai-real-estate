import { FC, Suspense, useEffect, useRef, useState } from 'react';
import { Vector3 } from 'three';
import { Canvas } from '@react-three/fiber';
import MapScene from './scene/mapScene';
import BuildingScene from './scene/buildingScene';
import Header from './ui/header';
import SceneTransition from './layout/sceneTransition';
import LoadingScreen from './layout/loadingScreen';
import Hint from './ui/hint';
import BuildingLayout from './layout/building';
import useExperienceStore from './store/useExperienceStore';
import { useProgress } from '@react-three/drei';
import useBuildStore from './store/useBuildStore';
import { useScale } from './hooks/useScale';

function App() {
  const showMap = useExperienceStore((state) => state.showMap);
  const showHint = useExperienceStore((state) => state.showHint);
  const toggleShowHint = useExperienceStore((state) => state.toggleShowHint);
  // toggleMap is triggered to start animation transition.
  // After that, inside useEffect in useScenetransition, showMap is toggled to change scene at the moment when screen is fully closed by <SceneTransition />
  const [toggleMapValue, setToggleMapValue] = useState(false);
  const changeScene = () => setToggleMapValue(!toggleMapValue)

  const transitionDiv = useRef<HTMLDivElement>(null);
  const startTransitionBetweenScenes = () => {
    transitionDiv.current?.classList.add('appear');
  };
  const endTransitionBetweenScenes = () => {
    transitionDiv.current?.classList.remove('appear');
    if (showMap && !showHint) setTimeout(() => toggleShowHint(), 1000);
  };

  const [showLoadingScreen, setShowLoadingScreen] = useState(true);

  return (
    <>
      <Suspense>
        <ReactThreeFiber
          changeScene={changeScene}
          showMap={showMap}
          endTransitionBetweenScenes={endTransitionBetweenScenes}
        />
        <Layout changeScene={changeScene} showMap={showMap} />
      </Suspense>
      {showLoadingScreen && <LoadingScreen setShowLoadingScreen={setShowLoadingScreen} />}
      <SceneTransition
        toggleMapValue={toggleMapValue}
        transitionDiv={transitionDiv}
        startTransitionBetweenScenes={startTransitionBetweenScenes}
      />
    </>
  );
}

interface ReactThreeFiberProps {
  changeScene: () => void;
  showMap: boolean;
  endTransitionBetweenScenes: () => void;
}

function BuildingSceneLoader() {
  const { progress, item } = useProgress()
  const setLoadingProgress = useBuildStore((state) => state.setLoadingProgress);

  useEffect(() => {
    if (item.includes('builds')) setLoadingProgress(progress)
  }, [progress])

  return <></>
}


const ReactThreeFiber: FC<ReactThreeFiberProps> = ({
  changeScene,
  showMap,
  endTransitionBetweenScenes,
}) => {
  const [MapSceneStartPos, setMapSceneStartPos] = useState(new Vector3(146.5, 100, 48));

  return (
    <div id="canvas-container">
      <Canvas>
        {showMap &&
          <MapScene
            changeScene={changeScene}
            MapSceneStartPos={MapSceneStartPos}
            setMapSceneStartPos={setMapSceneStartPos}
            //endTransitionBetweenScenes={endTransitionBetweenScenes}
          />
        }
        {!showMap && <Suspense fallback={<BuildingSceneLoader />}><BuildingScene endTransitionBetweenScenes={endTransitionBetweenScenes} /></Suspense>}
      </Canvas>
    </div>
  );
};

interface LayoutProps {
  changeScene: () => void;
  showMap: boolean;
}

const Layout: FC<LayoutProps> = ({ changeScene, showMap }) => {
  useScale()

  return (
    <>
      <Header />
      <Hint showMap={showMap} />
      {!showMap && <BuildingLayout changeScene={changeScene} />}
    </>
  );
};

export default App;
