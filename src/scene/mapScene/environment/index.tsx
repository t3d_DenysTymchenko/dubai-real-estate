import * as THREE from 'three'
import { groundMaterial } from '../../../utils/materials'
import { Environment } from '@react-three/drei'

const MapSceneEnvironment = () => {
  return (
    <>
      <color attach="background" args={[new THREE.Color(groundMaterial.color)]} />
      <Environment preset="night" />

      <ambientLight intensity={1.5} />
      <directionalLight intensity={1.5} position={[0, 5, 2]} />
    </>
  )
}

export default MapSceneEnvironment;
