import { FC, memo, useRef } from 'react';
import { Vector3 } from 'three';
import { MapControls } from 'three/examples/jsm/Addons.js';
import CameraAndControls from './cameraAndControls';
import MapSceneEnvironment from './environment';
import { Map } from './models/map';
import Pinpoints from './objects/pinpoints';
import DistrictLabels from './objects/districtLabels';
import useBuildStore from '../../store/useBuildStore';
import { waterMaterial } from '../../utils/materials';

interface MapSceneProps {
  changeScene: () => void
  MapSceneStartPos: Vector3;
  setMapSceneStartPos: React.Dispatch<React.SetStateAction<Vector3>>;
  //endTransitionBetweenScenes: () => void;
}

const MapScene: FC<MapSceneProps> = ({
  changeScene,
  MapSceneStartPos,
  setMapSceneStartPos,
  //endTransitionBetweenScenes
}) => {
  const setSelectedBuilding = useBuildStore(state => state.setSelectedBuilding);
  const controlsRef = useRef<MapControls>(null);
  const openBuildingScene = (building: string) => {
    // When scene is toggled the MapControls return to it's position on the begining.
    // To avoid that, I'm using MapSceneStartPos and setting its value right before scene transition.
    // This way user don't have any jumps and when he'll comeback to mapScene - user will continue moving from his latest position.
    // If this instructions isn't clear just comment line below to see the result:
    if (controlsRef.current) setMapSceneStartPos(controlsRef.current.target);
    setSelectedBuilding(building);
    changeScene();
  };

  return (
    <>
      <CameraAndControls
        controlsRef={controlsRef}
        MapSceneStartPos={MapSceneStartPos}
      />
      <MapSceneEnvironment />
      <Map>
        <Pinpoints openBuildingScene={openBuildingScene} controlsRef={controlsRef} />
      </Map>
      <DistrictLabels />
      {/* Mesh to hide water borders */}
      <mesh material={waterMaterial} position={[0, -1, 0]} rotation-x={-Math.PI / 2}>
        <planeGeometry args={[2000, 350]} />
      </mesh>
    </>
  );
};

export default memo(MapScene);
