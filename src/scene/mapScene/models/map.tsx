import * as THREE from 'three'
import { useGLTF } from '@react-three/drei'
import { GLTF } from 'three-stdlib'
import {
  buildingMaterial,
  greenZoneMaterial,
  groundMaterial,
  roadMaterial,
  waterMaterial
} from '../../../utils/materials';


export function Map(props: JSX.IntrinsicElements['group']) {
  return (
    <group rotation-y={-0.5}>
      <Part1 />
      <Part2 />
      <Part3 />
      {props.children}
    </group>
  )
}

type GLTFResultPart1 = GLTF & {
  nodes: {
    Road_plane: THREE.Mesh
    Plane: THREE.Mesh
    build_48001: THREE.Mesh
    build_48002: THREE.Mesh
    Cube: THREE.Mesh
    build_48004: THREE.Mesh
    build_48005: THREE.Mesh
    build_48006: THREE.Mesh
    build_48008: THREE.Mesh
    build_48007: THREE.Mesh
    build_48010: THREE.Mesh
    Cylinder: THREE.Mesh
    build_48009: THREE.Mesh
    Cube001: THREE.Mesh
    Cylinder001: THREE.Mesh
    build_48003: THREE.Mesh
    height_356_1001: THREE.Mesh
    build_48011: THREE.Mesh
    extra_build_by_turbocg_com003: THREE.Mesh
    build_38001: THREE.Mesh
    build_47001: THREE.Mesh
    build_48: THREE.Mesh
    build_48014: THREE.Mesh
    build_48012: THREE.Mesh
    build_48015: THREE.Mesh
    build_48016: THREE.Mesh
    build_2_2001: THREE.Mesh
    Cube002: THREE.Mesh
    mof1: THREE.Mesh
    Cube003: THREE.Mesh
    Cube004: THREE.Mesh
    bay001: THREE.Mesh
    buildin287001: THREE.Mesh
    buildin257001: THREE.Mesh
    buildin257002: THREE.Mesh
    highway_unclassified_1000_11001: THREE.Mesh
    highway_motorway_1001: THREE.Mesh
    highway_motorway_link_1000_3001: THREE.Mesh
    building77001: THREE.Mesh
    buildin257003: THREE.Mesh
    buildin257004: THREE.Mesh
    buildin257005: THREE.Mesh
    buildin257006: THREE.Mesh
    leisure_tr001: THREE.Mesh
    leisure_tr003: THREE.Mesh
    water_rbank001: THREE.Mesh
    leisure_tr002: THREE.Mesh
    leisure_tr004: THREE.Mesh
    leisure_tr005: THREE.Mesh
    leisure_tr006: THREE.Mesh
    leisure_tr007: THREE.Mesh
  }
  materials: {
    road: THREE.MeshPhysicalMaterial
    Ground_color: THREE.MeshPhysicalMaterial | THREE.MeshBasicMaterial
    ['Main_buildings.001']: THREE.MeshStandardMaterial | THREE.MeshBasicMaterial
    main_water: THREE.MeshStandardMaterial | THREE.MeshBasicMaterial
    ['road.001']: THREE.MeshPhysicalMaterial | THREE.MeshBasicMaterial
    Green_zone: THREE.MeshStandardMaterial | THREE.MeshBasicMaterial
  }
}

function Part1(props: JSX.IntrinsicElements['group']) {
  const { nodes, materials } = useGLTF('./models/map/part1.glb') as GLTFResultPart1

  materials.Ground_color = groundMaterial;
  materials['Main_buildings.001'] = buildingMaterial;
  materials['road.001'] = roadMaterial;
  materials.main_water = waterMaterial;
  materials.Green_zone = greenZoneMaterial;


  return (
    <group {...props} dispose={null}>
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Road_plane.geometry}
        material={materials.road}
        position={[-0.103, 0.009, -0.002]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Plane.geometry}
        material={materials.Ground_color}
        position={[0, -0.05, 0]}
        rotation={[0, 0.495, 0]}
        scale={122.701}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48001.geometry}
        material={materials['Main_buildings.001']}
        position={[148.856, -0.022, -44.552]}
        rotation={[Math.PI / 2, 0, -0.518]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48002.geometry}
        material={materials['Main_buildings.001']}
        position={[148.992, -0.111, -44.67]}
        rotation={[Math.PI / 2, 0, -0.518]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Cube.geometry}
        material={materials['Main_buildings.001']}
        position={[129.939, 0.118, -37.309]}
        rotation={[0, 0.706, 0]}
        scale={[2.488, 1, 1]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48004.geometry}
        material={materials['Main_buildings.001']}
        position={[148.856, -0.022, -44.552]}
        rotation={[Math.PI / 2, 0, -0.518]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48005.geometry}
        material={materials['Main_buildings.001']}
        position={[148.856, -0.024, -44.552]}
        rotation={[Math.PI / 2, 0, -0.518]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48006.geometry}
        material={materials['Main_buildings.001']}
        position={[148.638, -0.014, -44.37]}
        rotation={[Math.PI / 2, 0, -0.518]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48008.geometry}
        material={materials['Main_buildings.001']}
        position={[148.856, -0.022, -44.552]}
        rotation={[Math.PI / 2, 0, -0.518]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48007.geometry}
        material={materials['Main_buildings.001']}
        position={[148.856, -0.022, -44.552]}
        rotation={[Math.PI / 2, 0, -0.518]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48010.geometry}
        material={materials['Main_buildings.001']}
        position={[148.733, -0.032, -44.508]}
        rotation={[Math.PI / 2, 0, -0.518]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Cylinder.geometry}
        material={materials['Main_buildings.001']}
        position={[152.927, 1.264, -39.905]}
        rotation={[0, 1.04, 0]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48009.geometry}
        material={materials['Main_buildings.001']}
        position={[148.856, -0.022, -44.552]}
        rotation={[Math.PI / 2, 0, -0.518]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Cube001.geometry}
        material={materials['Main_buildings.001']}
        position={[130.888, 0.03, -37.413]}
        rotation={[0, 0.902, 0]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Cylinder001.geometry}
        material={materials['Main_buildings.001']}
        position={[131.027, 0.341, -37.382]}
        rotation={[0, 0.889, 0]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48003.geometry}
        material={materials['Main_buildings.001']}
        position={[148.856, -0.022, -44.552]}
        rotation={[Math.PI / 2, 0, -0.518]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.height_356_1001.geometry}
        material={materials['Main_buildings.001']}
        position={[159.762, 0.046, -60.645]}
        rotation={[Math.PI / 2, 0, 0.418]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48011.geometry}
        material={materials['Main_buildings.001']}
        position={[148.856, -0.022, -44.552]}
        rotation={[Math.PI / 2, 0, -0.518]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.extra_build_by_turbocg_com003.geometry}
        material={materials['Main_buildings.001']}
        position={[158.459, 0.005, -50.769]}
        rotation={[1.555, -0.022, 0.43]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_38001.geometry}
        material={materials['Main_buildings.001']}
        position={[168.043, 0.601, -59.079]}
        rotation={[1.571, -0.001, 0.402]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_47001.geometry}
        material={materials['Main_buildings.001']}
        position={[154.785, -0.043, -43.345]}
        rotation={[Math.PI / 2, 0, 0.408]}
        scale={3.754}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48.geometry}
        material={materials['Main_buildings.001']}
        position={[149.431, 2.502, -45.461]}
        rotation={[Math.PI / 2, 0, -0.513]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48014.geometry}
        material={materials['Main_buildings.001']}
        position={[156.056, 2.143, -58.355]}
        rotation={[Math.PI / 2, 0, -2.23]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48012.geometry}
        material={materials['Main_buildings.001']}
        position={[156.437, 2.143, -58.607]}
        rotation={[Math.PI / 2, 0, -0.493]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48015.geometry}
        material={materials['Main_buildings.001']}
        position={[148.856, -0.022, -44.552]}
        rotation={[Math.PI / 2, 0, -0.518]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_48016.geometry}
        material={materials['Main_buildings.001']}
        position={[158.034, 2.018, -55.619]}
        rotation={[Math.PI / 2, 0, -0.518]}
        scale={3.886}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.build_2_2001.geometry}
        material={materials['Main_buildings.001']}
        position={[31.41, 1.525, -21.634]}
        rotation={[Math.PI / 2, 0, 0.367]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Cube002.geometry}
        material={materials['Main_buildings.001']}
        position={[194.75, 0.772, -76.794]}
        rotation={[0, 0.777, 0]}
        scale={[1, 1, 0.884]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.mof1.geometry}
        material={materials['Main_buildings.001']}
        position={[167.827, 0.451, -64.299]}
        rotation={[1.52, 0.137, -0.525]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Cube003.geometry}
        material={materials['Main_buildings.001']}
        position={[-60.552, 1.038, 20.034]}
        rotation={[0, -1.472, 0]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.Cube004.geometry}
        material={materials['Main_buildings.001']}
        position={[181.539, -0.019, -72.395]}
        rotation={[0, 0.508, 0]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.bay001.geometry}
        material={materials.main_water}
        position={[114.212, 0.153, -93.737]}
        rotation={[Math.PI / 2, 0, 0.41]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.buildin287001.geometry}
        material={materials['Main_buildings.001']}
        position={[-118.341, 0.042, 62.965]}
        rotation={[Math.PI / 2, 0, 0.426]}
        scale={3.914}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.buildin257001.geometry}
        material={materials['Main_buildings.001']}
        position={[-86.121, 1.433, 108.963]}
        rotation={[Math.PI / 2, 0, 0]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.buildin257002.geometry}
        material={materials['Main_buildings.001']}
        position={[-110.301, -0.008, 54.626]}
        rotation={[Math.PI / 2, 0, 0.415]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.highway_unclassified_1000_11001.geometry}
        material={materials['road.001']}
        position={[-105.139, -0.11, 72.288]}
        rotation={[Math.PI / 2, 0, 0.908]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.highway_motorway_1001.geometry}
        material={materials['road.001']}
        position={[-119.203, -0.158, 66.136]}
        rotation={[1.57, 0.003, 0.439]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.highway_motorway_link_1000_3001.geometry}
        material={materials['road.001']}
        position={[-118.361, -0.112, 78.817]}
        rotation={[Math.PI / 2, 0, 0.419]}
        scale={3.673}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.building77001.geometry}
        material={materials['Main_buildings.001']}
        position={[-103.046, 1.43, 76.492]}
        rotation={[Math.PI / 2, 0, 0.929]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.buildin257003.geometry}
        material={materials['Main_buildings.001']}
        position={[-130.339, -0.174, 65.52]}
        rotation={[Math.PI / 2, 0, 0.436]}
        scale={5.303}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.buildin257004.geometry}
        material={materials['Main_buildings.001']}
        position={[-130.339, -0.174, 65.52]}
        rotation={[Math.PI / 2, 0, 0.436]}
        scale={5.303}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.buildin257005.geometry}
        material={materials['Main_buildings.001']}
        position={[-134.747, -0.174, 68.774]}
        rotation={[Math.PI / 2, 0, 1.993]}
        scale={5.303}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.buildin257006.geometry}
        material={materials['Main_buildings.001']}
        position={[-106.164, -0.165, 78.948]}
        rotation={[Math.PI / 2, 0, 1.847]}
        scale={5.303}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.leisure_tr001.geometry}
        material={materials.Green_zone}
        position={[42.423, -0.093, 61.228]}
        rotation={[Math.PI / 2, 0, 0.413]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.leisure_tr003.geometry}
        material={materials.Green_zone}
        position={[-19.047, -0.073, 31.591]}
        rotation={[Math.PI / 2, 0, 0.413]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.water_rbank001.geometry}
        material={materials.main_water}
        position={[149.008, -0.03, -42.766]}
        rotation={[Math.PI / 2, 0, 0.433]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.leisure_tr002.geometry}
        material={materials.Green_zone}
        position={[42.423, -0.049, 61.228]}
        rotation={[Math.PI / 2, 0, 0.413]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.leisure_tr004.geometry}
        material={materials.Green_zone}
        position={[189.844, -0.048, -75.387]}
        rotation={[Math.PI / 2, 0, 0.413]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.leisure_tr005.geometry}
        material={materials.Green_zone}
        position={[196.857, 0.001, -61.86]}
        rotation={[Math.PI / 2, 0, 0.413]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.leisure_tr006.geometry}
        material={materials.Green_zone}
        position={[187.034, -0.005, -53.38]}
        rotation={[Math.PI / 2, 0, 0.413]}
      />
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.leisure_tr007.geometry}
        material={materials.Green_zone}
        position={[42.423, -0.091, 61.228]}
        rotation={[Math.PI / 2, 0, 0.413]}
      />
    </group>
  )
}

type GLTFResultPart2 = GLTF & {
  nodes: {
    geo1001: THREE.Mesh
  }
  materials: {
    Main_buildings: THREE.MeshStandardMaterial
  }
}

function Part2(props: JSX.IntrinsicElements['group']) {
  const { nodes, materials } = useGLTF('./models/map/part2.glb') as GLTFResultPart2
  materials.Main_buildings = buildingMaterial;

  return (
    <group {...props} dispose={null}>
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.geo1001.geometry}
        material={materials.Main_buildings}
        position={[-0.01, -0.017, -0.045]}
        rotation={[-0.008, 0, 0.009]}
      />
    </group>
  )
}

type GLTFResultPart3 = GLTF & {
  nodes: {
    geo1: THREE.Mesh
  }
  materials: {
    Main_buildings: THREE.MeshStandardMaterial
  }
}

export function Part3(props: JSX.IntrinsicElements['group']) {
  const { nodes, materials } = useGLTF('./models/map/part3.glb') as GLTFResultPart3
  materials.Main_buildings = buildingMaterial;

  return (
    <group {...props} dispose={null}>
      <mesh
        castShadow
        receiveShadow
        geometry={nodes.geo1.geometry}
        material={materials.Main_buildings}
        position={[-0.01, -0.017, -0.045]}
        rotation={[-0.008, 0, 0.009]}
      />
    </group>
  )
}

useGLTF.preload('./models/map/part3.glb')
useGLTF.preload('./models/map/part2.glb')
useGLTF.preload('./models/map/part1.glb')
