import { forwardRef } from 'react';
import * as THREE from 'three';
import { GroupProps } from '@react-three/fiber';
import { useGLTF } from '@react-three/drei';
import { GLTF } from 'three-stdlib';

// Define the type for the GLTF result
type GLTFResult = GLTF & {
  nodes: {
    Cylinder: THREE.Mesh
  }
  materials: {
    ['Material.001']: THREE.MeshStandardMaterial
  }
}

interface CustomGroupProps extends GroupProps {
  material: THREE.Material
}

const PinpointModel = forwardRef<THREE.Group, CustomGroupProps>((props, ref) => {
  const { nodes } = useGLTF('./models/pinpoint.glb') as GLTFResult;

  return (
    <group {...props} ref={ref}>
      <mesh
        geometry={nodes.Cylinder.geometry}
        material={props.material}
        rotation={[-Math.PI / 2, 0, -Math.PI / 1.2]}
        scale={[1, 0.355, 1]}
      />
    </group>
  );
});

export default PinpointModel;
useGLTF.preload('./models/pinpoint.glb');
