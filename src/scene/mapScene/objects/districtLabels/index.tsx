import { FC } from 'react';
import { Text } from '@react-three/drei';
import { Vector3 } from 'three';

const DistrictLabels: FC = () => {
  const labels = [
    { text: 'PALM JUMEIRAH', position: new Vector3(-19, 2, -60) },
    { text: 'Dubai Marina', position: new Vector3(-55, 2, 8), rotationZ: -0.05 },
    { text: 'DUBAI ISLANDS', position: new Vector3(285, 2, -34) },
    { text: 'DOWNTOWN', position: new Vector3(148, 2, 39) },
    { text: 'MEYDAN', position: new Vector3(135, 2, 88) },
    { text: 'JADDAF WATERFRONT', position: new Vector3(209, 2, 75) },
    { text: 'DUBAI HILLS', position: new Vector3(48, 2, 75) },
    { text: 'ARJAN', position: new Vector3(0, 2, 95) },
    { text: 'JVC', position: new Vector3(-27, 2, 88) },
    { text: 'AIRPORT', position: new Vector3(267.75, 2, 65), rotationZ: 0.1 },
  ]

  return (
    <>
      {labels.map((label, index) => (
        <Text
          key={index}
          fontSize={1}
          fontWeight={800}
          letterSpacing={0.1}
          outlineBlur={0.3}
          outlineColor='#fff'
          position={label.position}
          rotation-x={-Math.PI / 2}
          rotation-z={label.rotationZ ?? 0}
          scale={4.5}
          color='#454545'
        >
          {label.text}
        </Text>
      ))}
    </>
  )
}

export default DistrictLabels;
