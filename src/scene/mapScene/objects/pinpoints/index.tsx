import { FC } from 'react';
import Pinpoint from './pinpoint';
import { MapControls } from 'three/examples/jsm/Addons.js';
import { Vector3 } from 'three';

type PinpointsProps = {
  openBuildingScene: Function;
  controlsRef: React.RefObject<MapControls>;
}

const Pinpoints: FC<PinpointsProps> = ({ openBuildingScene, controlsRef }) => {
  const pinpointsArgs = [
    { buildingName: 'sls', position: [-60, 0, -15], cameraGoTo: new Vector3(-15, 0, -40) },
    { buildingName: 'rise', position: [20.5, 0, 70], cameraGoTo: new Vector3(10, 0, 40) },
    { buildingName: 'roma', position: [24, 0, 73.5], cameraGoTo: new Vector3(10, 0, 40) },
    { buildingName: 'sapphire', position: [17.5, 0, 85.6], cameraGoTo: new Vector3(10, 0, 40) },
    // Mövenpick Residences'
    //{ buildingName: 'coming-soon', position: [60, 0, 88.5] },
    { buildingName: 'hyde', position: [72, 0, 43.7], cameraGoTo: new Vector3(43, 0, 40) },
    { buildingName: 'ayana', position: [170, 0, 3.4], cameraGoTo: new Vector3(148, 0, 55) },
    // Banyan Tree
    { buildingName: 'coming-soon', position: [154.5, 0, -32.3] },
    // ???
    { buildingName: 'coming-soon', position: [-40, 0, 25] },
    { buildingName: 'coming-soon', position: [232, 0, -48] },
    { buildingName: 'coming-soon', position: [230, 0, -52] },
    { buildingName: '25h', position: [143.4, 0, -38], cameraGoTo: new Vector3(145, 0, 13) },
    { buildingName: 'azura', position: [212, 0, -129.5],  cameraGoTo: new Vector3(205, 0, -30) },
  ]

  return (
    <>
      {
        pinpointsArgs.map((pinpoint, index) => (
          <Pinpoint
            key={index}
            buildingName={pinpoint.buildingName}
            position={pinpoint.position}
            cameraGoTo={pinpoint.cameraGoTo}
            openBuildingScene={openBuildingScene}
            controlsRef={controlsRef}
          />
        ))
      }
    </>
  );
}

export default Pinpoints;
