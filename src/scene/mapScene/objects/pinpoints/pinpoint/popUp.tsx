import { FC } from 'react';
import { Billboard, Html } from '@react-three/drei';
import BuildingInfoPopUp from '../../../../../ui/building/info/popUp';

type PopUpProps = {
  buildingName: string;
  togglePopUp: () => void;
  openBuildingScene: Function;
}
const PopUp: FC<PopUpProps> = ({ buildingName, togglePopUp, openBuildingScene }) => {
  return (
    <Billboard scale={4} position={[0, 21, -3]}>
      <Html as='div' center className='building-info-pop-up not-selectable' rotation-x={Math.PI / 8}>
        <BuildingInfoPopUp buildingName={buildingName} togglePopUp={togglePopUp} openBuildingScene={openBuildingScene} />
      </Html>
    </Billboard>
  )
}

export default PopUp;
