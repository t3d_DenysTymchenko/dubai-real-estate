import { Billboard, Html } from '@react-three/drei';
import { FC } from 'react';
import useExperienceStore from '../../../../../store/useExperienceStore';

type LogoLabelProps = {
  logo: string;
  togglePopUp: () => void;
}

const LogoLabel: FC<LogoLabelProps> = ({ logo, togglePopUp }) => {
  const startTransitionEnded = useExperienceStore(state => state.startTransitionEnded);
  const LogoLabelStyles: React.CSSProperties = {
    width: '150px',
    height: '75px',
    backgroundImage: `url(${logo})`,
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    cursor: 'pointer',
    borderRadius: '15px',
    border: '1px solid white',
    scale: `${startTransitionEnded ? 'var(--scale)' : '0'}`,
    transition: 'scale 0.8s ease-in-out',
  };

  const ComingSoonLabelStyles: React.CSSProperties = {
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    backdropFilter: 'blur(20px)',
    cursor: 'pointer',
    padding: '8px 16px',
    borderRadius: '15px',
    border: '1px solid white',
    scale: `${startTransitionEnded ? 'var(--scale)' : '0'}`,
    transition: 'scale 0.8s ease-in-out',
  };

  const isComingSoon = logo.includes('coming-soon')

  return (
    <Billboard position-y={10}>
      <Html center className="not-selectable">
        <div style={isComingSoon ? ComingSoonLabelStyles : LogoLabelStyles} onClick={togglePopUp}>
          {isComingSoon && <img src={logo} alt='Building logo' width={'110px'} height={'50px'} style={{ pointerEvents: 'none' }} />}
        </div>
      </Html>
    </Billboard>
  );
};

export default LogoLabel;
