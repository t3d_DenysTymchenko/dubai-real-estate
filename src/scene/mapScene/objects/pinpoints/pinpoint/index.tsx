import { FC, useEffect, useRef, useState } from 'react';
import * as THREE from 'three';
import PopUp from './popUp';
import LogoLabel from './logoLabel';
import useAnimatePinpoint from '../../../../../hooks/useAnimatePinpoint';
import PinpointModel from '../../../models/pinpoint';
import { MapControls } from 'three/examples/jsm/Addons.js';
import gsap from 'gsap';

type PinpointProps = {
  buildingName: string;
  position: number[];
  cameraGoTo: THREE.Vector3 | undefined;
  openBuildingScene: Function;
  controlsRef: React.RefObject<MapControls>;
}

const Pinpoint: FC<PinpointProps> = ({
  buildingName,
  position,
  cameraGoTo,
  openBuildingScene,
  controlsRef
}) => {
  const [hovered, setHovered] = useState(false);
  const [clicked, setClicked] = useState(false);
  const pinpoint = useRef<THREE.Group>(null);

  const togglePopUp = () => {
    if(!clicked && cameraGoTo) {
      //@ts-ignore
      gsap.to(controlsRef.current?.target, {
        duration: 0.3,
        ease: 'sine.inOut',
        x: cameraGoTo.x,
        y: cameraGoTo.y,
        z: cameraGoTo.z,
      });
    }
    if (buildingName !== 'coming-soon') setClicked(!clicked)
  };

  useAnimatePinpoint(hovered, clicked, pinpoint);

  useEffect(() => {
    if (hovered) document.body.style.cursor = 'pointer';
    return () => {
      document.body.style.cursor = 'auto';
    };
  }, [hovered]);

  return (
    <group position={new THREE.Vector3(...position)}>
      <PinpointModel
        ref={pinpoint}
        material={new THREE.MeshStandardMaterial({ color: clicked ? 'purple' : '#854C89' })}
        onPointerOver={() => setHovered(true)}
        onPointerOut={() => setHovered(false)}
        onClick={togglePopUp}
      />

      {!clicked ?
        <LogoLabel logo={`./logos/map/${buildingName}.svg`} togglePopUp={togglePopUp} /> :
        <PopUp buildingName={buildingName} togglePopUp={togglePopUp} openBuildingScene={openBuildingScene} />
      }
    </group>
  );
};

export default Pinpoint;
