import { FC, memo, useRef, useState } from 'react';
import * as THREE from 'three';
import { MapControls as ThreeMapControls } from 'three/examples/jsm/controls/MapControls.js';
import { MapControls, PerspectiveCamera } from '@react-three/drei';
import { useCameraTransitionOnStart } from '../../../hooks/useCameraTransitionOnStart';
import useExperienceStore from '../../../store/useExperienceStore';
import {
  DEFAULT_CONTROLS_TARGET_MAX,
  DEFAULT_CONTROLS_TARGET_MIN,
  MOBILE_CONTROLS_TARGET_MAX,
  MOBILE_CONTROLS_TARGET_MIN
} from '../../../utils/constants.ts';
//import { useControls } from 'leva';

interface CameraAndControlsProps {
  controlsRef: React.RefObject<ThreeMapControls>;
  MapSceneStartPos: THREE.Vector3;
}

const CameraAndControls: FC<CameraAndControlsProps> = ({ controlsRef, MapSceneStartPos }) => {
  //const { x, y, z } = useControls(
  //  {
  //    x: { value: 146.5, min: -500, max: 500, step: 0.1 },
  //    y: { value: 0, min: -500, max: 500, step: 0.1 },
  //    z: { value: 48, min: -500, max: 500, step: 0.1 },
  //  }
  //)
  const startTransitionEnded = useExperienceStore(state => state.startTransitionEnded);
  const [applyPolarAngle, setApplyPolarAngle] = useState(startTransitionEnded);
  const cameraRef = useRef<THREE.PerspectiveCamera>(null);
  let controlsTargetMin = window.innerWidth > window.innerHeight ? DEFAULT_CONTROLS_TARGET_MIN : MOBILE_CONTROLS_TARGET_MIN;
  let controlsTargetMax = window.innerWidth > window.innerHeight ? DEFAULT_CONTROLS_TARGET_MAX : MOBILE_CONTROLS_TARGET_MAX;

  window.addEventListener('resize', () => {
    controlsTargetMin = window.innerWidth > window.innerHeight ? DEFAULT_CONTROLS_TARGET_MIN : MOBILE_CONTROLS_TARGET_MIN;
    controlsTargetMax = window.innerWidth > window.innerHeight ? DEFAULT_CONTROLS_TARGET_MAX : MOBILE_CONTROLS_TARGET_MAX;
  });

  useCameraTransitionOnStart(
    startTransitionEnded,
    cameraRef,
    controlsRef,
    setApplyPolarAngle
  );

  return (
    <>
      <PerspectiveCamera
        ref={cameraRef}
        makeDefault
        fov={50}
        near={1}
        far={520}
        position={!startTransitionEnded ? new THREE.Vector3(105, 255, -8) : new THREE.Vector3(146.5, 100, 48)}
      />
      <MapControls
        /*@ts-ignore*/
        ref={controlsRef}
        makeDefault
        enabled={applyPolarAngle}
        enableRotate={false}
        maxDistance={startTransitionEnded ? 105 : undefined}
        minDistance={startTransitionEnded ? 85 : undefined}
        maxPolarAngle={applyPolarAngle ? 0.76 : undefined}
        minPolarAngle={applyPolarAngle ? 0.76 : undefined}
        maxAzimuthAngle={0}
        minAzimuthAngle={0}
        target={!startTransitionEnded ? new THREE.Vector3(105, 0, -8) : [MapSceneStartPos.x, 0, MapSceneStartPos.z]}
        onChange={(e) => {
          if (!startTransitionEnded || !e?.target) return;
          const controls = e.target as ThreeMapControls;
          controls.target.clamp(controlsTargetMin, controlsTargetMax);
        }}
      />
    </>
  );
};

export default memo(CameraAndControls);
