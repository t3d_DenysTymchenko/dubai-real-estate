import { OrbitControls, PerspectiveCamera } from '@react-three/drei'
//import { useThree } from '@react-three/fiber';
//import { useControls } from 'leva';
import { CameraSettings } from '../../../utils/buildings';
import { FC } from 'react';

type CameraAndControlsProps = {
  cameraSettings: CameraSettings,
}
const CameraAndControls: FC<CameraAndControlsProps> = ({ cameraSettings }) => {
  const { target, position, fov } = cameraSettings;

  //const { x, y, z, lookx, looky, lookz, fov, far, rot } = useControls({
  //  x: { value: 500, min: -500, max: 500, step: 0.1 },
  //  y: { value: 100, min: -500, max: 500, step: 0.1 },
  //  z: { value: -275, min: -500, max: 500, step: 0.1 },
  //  lookx: { value: 500, min: -1500, max: 1500, step: 0.1 },
  //  looky: { value: 5, min: -1500, max: 1500, step: 0.1 },
  //  lookz: { value: 420, min: -1500, max: 1500, step: 0.1 },
  //  fov: { value: 60, min: 0, max: 100, step: 1 },
  //  far: { value: 800, min: 0, max: 2000, step: 1 },
  //  rot: { value: -2.2, min: -5, max: 5, step: 0.01 }
  //})

  return (
    <>
      <OrbitControls
        target={target}
        maxPolarAngle={Math.PI / 2.1}
        maxDistance={125}
        minDistance={75}
        enablePan={false}
        makeDefault
      />

      <PerspectiveCamera
        makeDefault
        position={position}
        fov={fov}
        far={800}
      />
    </>
  )
}

export default CameraAndControls;
