import { FC, ReactNode } from 'react';
import { Text } from '@react-three/drei';

interface RoadLabelProps {
  position: [x: number, y: number, z: number];
  rotationZ: number;
  look: 'forward' | 'backward';
  children: ReactNode;
}

const RoadLabel: FC<RoadLabelProps> = ({ position, rotationZ, look, children }) => {
  return (
    <Text
      fontSize={1}
      fontWeight={800}
      letterSpacing={0.1}
      outlineBlur={0.2}
      outlineColor='#fff'
      position={position}
      rotation-x={look === 'forward' ? -Math.PI / 2 : Math.PI / 2 }
      rotation-z={rotationZ}
      scale={4.5}
      color='black'
    >
      {children}
    </Text>
  );
};

export default RoadLabel;
