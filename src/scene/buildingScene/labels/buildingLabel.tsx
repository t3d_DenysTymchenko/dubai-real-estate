import { Billboard, Text } from '@react-three/drei';
import { labelMaterial } from '../../../utils/materials';
import { FC, ReactNode } from 'react';

interface RoadLabelProps {
  position: [x: number, y: number, z: number];
  rotationY: number;
  bannerWidth: number;
  children: ReactNode;
}

const BuildingLabel: FC<RoadLabelProps> = ({ position, rotationY, bannerWidth, children }) => {
  return (
    <group position={position} rotation-y={rotationY}>
      <Billboard position-y={5.5}>
        <Text fontSize={5} color="white">
          <mesh material={labelMaterial}>
            <planeGeometry args={[bannerWidth, 7.5]} />
          </mesh>
          {children}
        </Text>
      </Billboard>
      <group>
        <mesh position={[0, -4.3, 0]} material={labelMaterial}>
          <planeGeometry args={[1, 13]} />
        </mesh>
        <mesh scale={0.5} position={[0, -14, 0]} material={labelMaterial}>
          <ringGeometry args={[6.5, 5, 30, 1, 0, 7]} />
        </mesh>
      </group>
    </group>
  )
}

export default BuildingLabel;
