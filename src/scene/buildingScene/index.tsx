import { FC, useEffect } from 'react';
import { useGLTF } from '@react-three/drei';
import CameraAndControls from './cameraAndControls';
import BuildingSceneEnvironment from './environment';
import BuildingLabel from './labels/buildingLabel';
import RoadLabel from './labels/roadLabel';
import { useChangeBuildEnvironmentMaterials } from '../../hooks/useChangeBuildEnvironmentMaterials';
import { Building, buildings } from '../../utils/buildings';
import useBuildStore from '../../store/useBuildStore';

interface BuildingSceneProps {
  endTransitionBetweenScenes: () => void;
}

const BuildingScene: FC<BuildingSceneProps> = ({ endTransitionBetweenScenes }) => {
  const selectedBuilding = useBuildStore(state => state.selectedBuilding);
  const homeData = buildings[selectedBuilding] as Building;
  const model = homeData.no_model ? null : useGLTF(`./models/builds/${selectedBuilding}.glb`);
  console.log(homeData)
  useEffect(() => {
    if (homeData.no_model || model?.scene) {
      endTransitionBetweenScenes();
      if (model?.scene) useChangeBuildEnvironmentMaterials(model);
    }
  }, [model]);

  return (
    (!homeData.no_model && model?.scene && (
      <>
        <CameraAndControls cameraSettings={homeData.camera_settings!} />
        <BuildingSceneEnvironment scene={model.scene} />
        {homeData.road_labels.map((label, index: number) => (
          <RoadLabel
            key={index}
            look={label.look}
            position={label.position}
            rotationZ={label.rotationZ}
          >
            {label.text}
          </RoadLabel>
        ))}
        {homeData.building_labels.map((label, index: number) => (
          <BuildingLabel
            key={index}
            position={label.position}
            rotationY={label.rotationY}
            bannerWidth={label.banner_width}
          >
            {label.text}
          </BuildingLabel>
        ))}
        <primitive object={model.scene} />
      </>
    ))
  );
};

export default BuildingScene;
