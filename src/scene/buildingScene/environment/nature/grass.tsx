import { Instance, Instances, useGLTF } from '@react-three/drei';
import { FC, useEffect, useState } from 'react';
import { Group, Mesh, Object3DEventMap } from 'three';
import { InstanceInfo } from '.';

interface GrassProps {
  scene: Group<Object3DEventMap>;
  modelName: string;
}

const Grass: FC<GrassProps> = ({ scene, modelName }) => {
  const { nodes, materials } = useGLTF(`./models/environment/${modelName}.glb`);
  const geometry = (nodes.grass_green_lod_1 as Mesh).geometry;
  const material = materials.grass_green;
  const [instancesInfo, setInstancesInfo] = useState<InstanceInfo[]>([])

  useEffect(() => {
    if (nodes && materials) {
      const info: InstanceInfo[] = [];
      scene.traverse((child) => {
        if (child.name.includes(modelName)) {
          const { position, rotation } = child
          info.push({ position, rotation });
        }
      });

      setInstancesInfo(info)
    }
  }, [nodes, materials]);

  return (
    <Instances range={instancesInfo.length} material={material} geometry={geometry}>
      {instancesInfo.map(({ position, rotation }, i) => (
        <Instance key={i} position={position} rotation={rotation} scale={0.4} />
      ))}
    </Instances>
  )
}

export default Grass;
