import { Instance, Instances, useGLTF } from '@react-three/drei';
import { FC, useEffect, useState } from 'react';
import { Group, Mesh, Object3DEventMap } from 'three';
import { InstanceInfo } from '.';

interface TreeProps {
  scene: Group<Object3DEventMap>;
  modelName: string;
  nodeToSelect: string;
  scale: number;
}

const Tree: FC<TreeProps> = ({
  scene,
  modelName,
  nodeToSelect,
  scale
}) => {
  const { nodes, materials } = useGLTF(`./models/environment/${modelName}.glb`);
  const tree_pole = (nodes[nodeToSelect].children[0] as Mesh);
  const tree_leaves = (nodes[nodeToSelect].children[1] as Mesh);
  const [instancesInfo, setInstancesInfo] = useState<InstanceInfo[]>([])

  useEffect(() => {
    if (nodes && materials) {
      const info: InstanceInfo[] = [];
      scene.traverse((child) => {
        if (child.name.toLowerCase().includes(modelName)) {
          const { position, rotation } = child
          info.push({ position, rotation });
        }
      });

      setInstancesInfo(info)
    }
  }, [nodes, materials]);

  return (
    <group>
      <Instances range={instancesInfo.length} material={tree_pole.material} geometry={tree_pole.geometry}>
        {instancesInfo.map(({ position, rotation }, i) => (
          <Instance key={i} position={position} rotation={rotation} scale={scale} />
        ))}
      </Instances>
      <Instances range={instancesInfo.length} material={tree_leaves.material} geometry={tree_leaves.geometry}>
        {instancesInfo.map(({ position, rotation }, i) => (
          <Instance key={i} position={position} rotation={rotation} scale={scale} />
        ))}
      </Instances>
    </group>
  )
}

export default Tree;
