import { FC } from 'react';
import { Euler, Vector3 } from 'three';
import { BuildingSceneEnvironmentProps } from '..';
import Tree from './tree';
import Grass from './grass';

export interface InstanceInfo {
  position: Vector3,
  rotation: Euler,
}

const Nature: FC<BuildingSceneEnvironmentProps> = ({ scene }) => {
  return (
    <group position={[0, -2, 0]}>
      <Tree scene={scene} modelName='tree_v1' nodeToSelect='Tree_LOD_2' scale={1.3} />
      <Tree scene={scene} modelName='tree_v2' nodeToSelect='Beech_01' scale={0.5} />
      <Tree scene={scene} modelName='tree_v3' nodeToSelect='Maple' scale={0.4} />
      <Tree scene={scene} modelName='palm' nodeToSelect='tree001' scale={0.3} />
      <Grass scene={scene} modelName='grass_green' />
      <Grass scene={scene} modelName='grass_light_green' />
      <Grass scene={scene} modelName='grass_red' />
    </group>
  )
}

export default Nature;
