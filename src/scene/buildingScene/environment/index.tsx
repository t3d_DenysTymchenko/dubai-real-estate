import { Group, Object3DEventMap } from 'three'
import { Environment } from '@react-three/drei'
import { BrightnessContrast, EffectComposer } from '@react-three/postprocessing'
import Skydome from './skydome'
import Nature from './nature'
import { FC } from 'react'

export interface BuildingSceneEnvironmentProps {
  scene: Group<Object3DEventMap>
}

const BuildingSceneEnvironment: FC<BuildingSceneEnvironmentProps> = ({ scene }) => {
  return (
    <>
      <Environment preset='city' environmentIntensity={0.75} />
      <Skydome />
      <Nature scene={scene} />

      <EffectComposer enableNormalPass={true}>
        <BrightnessContrast />
      </EffectComposer>

      <ambientLight intensity={1} />
    </>
  )
}

export default BuildingSceneEnvironment;
