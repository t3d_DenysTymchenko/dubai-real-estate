import { useRef } from 'react'
import * as THREE from 'three'
import { useFrame } from '@react-three/fiber'
import { useTexture } from '@react-three/drei'

const Skydome = () => {
  const texture = useTexture('./background.jpg')
  const skydomeRef = useRef<THREE.Mesh>(null)

  useFrame(({ }, delta) => {
    if (skydomeRef.current) {
      skydomeRef.current.rotation.y += delta / 250
    }
  })

  return (
    <mesh ref={skydomeRef}>
      <sphereGeometry args={[650, 64, 32]} />
      <meshBasicMaterial map={texture} side={THREE.BackSide} />
    </mesh>
  )
}

export default Skydome;
