import { useFrame } from '@react-three/fiber';
import { RefObject } from 'react';
import * as THREE from 'three';

const useAnimatePinpoint = (hovered: boolean, clicked: boolean, pinpoint: RefObject<THREE.Group>) => {
  const color = new THREE.Color('red');

  const animateOnHover = () => {
    const pinpointMaterial: THREE.MeshStandardMaterial = (pinpoint.current?.children[0] as THREE.Mesh).material as THREE.MeshStandardMaterial;
    pinpointMaterial?.color.lerp(color.set(hovered ? 'purple' : '#854C89'), 0.2);
  };

  useFrame(({ }, delta) => {
    if (!pinpoint.current) return;
    pinpoint.current!.rotation.y += delta;
    if(!clicked) animateOnHover();
  });
};

export default useAnimatePinpoint;
