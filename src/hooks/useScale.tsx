export const useScale = () => {
  const root: HTMLElement | null = document.querySelector(':root');
  
  const updateScale = () => {
    const scaleValue = window.innerWidth > 2000 ? window.innerWidth / 1800 : 1;
    root?.style.setProperty('--scale', `${scaleValue}`);
  }
  
  updateScale()
  window.addEventListener('resize', () => updateScale())
}
