import { useEffect } from 'react';
import useExperienceStore from '../store/useExperienceStore.tsx';
import useBuildStore from '../store/useBuildStore.tsx';

const useUpdateURL = () => {
  const showMap = useExperienceStore((state) => state.showMap);
  const selectedBuilding = useBuildStore((state) => state.selectedBuilding);

  const url = new URL(window.location.href);
  const params = url.searchParams;

  useEffect(() => {
    showMap ? params.delete('build') : params.set('build', selectedBuilding.toLowerCase());
    window.history.pushState({}, '', url);
  }, [showMap, selectedBuilding]);
};

export default useUpdateURL;
