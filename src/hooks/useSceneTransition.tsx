import { useEffect, useState } from 'react';
import { SceneTransitionProps } from '../layout/sceneTransition';
import useUpdateURL from './useUpdateURL.tsx';
import useExperienceStore from '../store/useExperienceStore';

const useSceneTransition = ({
  toggleMapValue,
  startTransitionBetweenScenes,
}: SceneTransitionProps): void => {
  const showMap = useExperienceStore((state) => state.showMap);
  const toggleShowMap = useExperienceStore((state) => state.toggleShowMap);
  const toggleShowHint = useExperienceStore((state) => state.toggleShowHint);
  const [canBeAnimated, setCanBeAnimated] = useState<boolean>(false);

  useUpdateURL();

  const changeScene = () => {
    setTimeout(() => toggleShowMap(), 950);
  };

  useEffect(() => {
    if (!canBeAnimated) {
      setCanBeAnimated(true);
      return;
    }

    if (showMap) toggleShowHint();
    startTransitionBetweenScenes();
    changeScene();
  }, [toggleMapValue]);
};

export default useSceneTransition;
