import { useEffect } from 'react';
import { PerspectiveCamera } from 'three';
import { MapControls } from 'three/examples/jsm/Addons.js';
import gsap from 'gsap';
import useExperienceStore from '../store/useExperienceStore';

export const useCameraTransitionOnStart = (
  startTransitionEnded: boolean,
  camera:  React.RefObject<PerspectiveCamera>,
  controls: React.RefObject<MapControls>,
  setApplyPolarAngle: React.Dispatch<React.SetStateAction<boolean>>,
) => {
  const isExperienceStarted = useExperienceStore((store) => store.isExperienceStarted);
  const endStartTransition = useExperienceStore((state) => state.endStartTransition);
  const toggleShowHint = useExperienceStore((state) => state.toggleShowHint);

  const animateTransition = () => {
    setTimeout(() => {
      gsap.to(camera.current!.position, {
        duration: 1.5,
        ease: 'power1.inOut',
        x: 146.5,
        y: 100,
        z: 48,
        onComplete: () => setNewCameraOptions(),
      });

      gsap.to(controls.current!.target, {
        duration: 1.5,
        ease: 'power1.inOut',
        x: 146.5,
        y: 0,
        z: 48,
      });
    }, 500);
  };

  const setNewCameraOptions = () => {
    endStartTransition();
    //@ts-ignore
    controls.current.setPolarAngle(0.8);
    setTimeout(() => {
      setApplyPolarAngle(true);
      toggleShowHint();
    }, 1850);
  };

  useEffect(() => {
    if (startTransitionEnded || (!isExperienceStarted || !camera.current || !controls.current)) return;
    animateTransition();
  }, [isExperienceStarted, camera, controls]);
};
