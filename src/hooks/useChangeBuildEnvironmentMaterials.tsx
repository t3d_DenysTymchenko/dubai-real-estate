import * as THREE from 'three';
import { ObjectMap } from '@react-three/fiber';
import { GLTF } from 'three-stdlib';
import { buildingMaterial_buildingScene, groundMaterial_buildingScene, roadMaterial_buildingScene } from '../utils/materials';

export const useChangeBuildEnvironmentMaterials = (environment: GLTF & ObjectMap) => {
  console.log(environment)
  environment.scene.traverse(child => {
    //console.log(child.name)
    //console.log(child.material?.name)
    const childAsMesh = child as THREE.Mesh;
    const materials = childAsMesh.material as THREE.MeshStandardMaterial 
    if (childAsMesh.name.toLowerCase() === 'build' || materials?.name.toLowerCase() === 'build') {
      childAsMesh.material = buildingMaterial_buildingScene;
    }

    if (childAsMesh.name === 'road' ||  materials?.name === 'road') {
      childAsMesh.material = roadMaterial_buildingScene;
    }

    if (childAsMesh.name === 'land' ||  materials?.name === 'land') {
      childAsMesh.material = groundMaterial_buildingScene;
    }
  });
}
