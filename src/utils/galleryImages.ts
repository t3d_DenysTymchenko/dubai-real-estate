export enum ImagesType {
  EXTERIOR = 'exterior',
  INTERIOR = 'interior',
}

type GalleryImages = {
  [buildingName: string]: {
    [ImagesType.EXTERIOR]: string[],
    [ImagesType.INTERIOR]: string[],
  };
}

const getBuildingName = (path: string): string => {
  const regex = /gallery\/([^/]+)\//;
  const match = path.match(regex);
  return match![1]
}

const galleryImages: GalleryImages = {}
const galleryImagesSrc = import.meta.glob('../../public/gallery/**/*.jpg')

for (const src in galleryImagesSrc) {
  const imageType = src.includes(ImagesType.EXTERIOR) ? ImagesType.EXTERIOR : ImagesType.INTERIOR;
  const buildingName = getBuildingName(src)
  const correctSrc = src.replace('../../public/', './')

  if (!galleryImages[buildingName]) {
    galleryImages[buildingName] = {
      [ImagesType.EXTERIOR]: [],
      [ImagesType.INTERIOR]: [],
    }
  }

  galleryImages[buildingName][imageType].push(correctSrc)
}

export default galleryImages
