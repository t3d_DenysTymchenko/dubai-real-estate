import { Vector3 } from 'three';

export const DEFAULT_CONTROLS_TARGET_MIN = new Vector3(-15, 0, -125);
export const MOBILE_CONTROLS_TARGET_MIN = new Vector3(-80, 0, -125);
export const DEFAULT_CONTROLS_TARGET_MAX = new Vector3(210, 0, 62);
export const MOBILE_CONTROLS_TARGET_MAX = new Vector3(275, 0, 62);
