import { DoubleSide, MeshBasicMaterial, MeshStandardMaterial } from 'three';

// Default
export const buildingMaterial: MeshStandardMaterial = new MeshStandardMaterial({ color: '#969696' });
export const groundMaterial: MeshBasicMaterial = new MeshBasicMaterial({ color: '#c6c6c6' });
export const waterMaterial: MeshBasicMaterial = new MeshBasicMaterial({ color: '#6294aa' });
export const roadMaterial: MeshBasicMaterial = new MeshBasicMaterial({ color: '#525050' });
export const greenZoneMaterial: MeshBasicMaterial = new MeshBasicMaterial({ color: '#739a69' });

// Building scene
export const buildingMaterial_buildingScene: MeshStandardMaterial = new MeshStandardMaterial({ color: '#bdbebf', transparent: true, opacity: 0.8 })
export const roadMaterial_buildingScene: MeshStandardMaterial = new MeshStandardMaterial({ color: 'gray' })
export const groundMaterial_buildingScene: MeshStandardMaterial = new MeshStandardMaterial({ color: '#b2b2b2' })
export const waterMaterial_buildingScene: MeshStandardMaterial = new MeshStandardMaterial({ color: '#6294aa', roughness: 0.75, metalness: 1 });

// Other
export const labelMaterial: MeshBasicMaterial = new MeshBasicMaterial({ color: '#313638', side: DoubleSide })
