import { useState } from 'react';
import './index.scss'

const Select = () => {
  const [showOptions, setShowOptions] = useState(false);

  return (
    <div className='city-selector' onClick={() => setShowOptions(!showOptions)}>
      <span className='selected-value'>Dubai</span>
      {showOptions && <div className='options'>
        {/*<option>Dubai</option>*/}
        <option disabled>Sharjah</option>
        <option disabled>Casablanca</option>
      </div>}
    </div>
  )
}

export default Select;
