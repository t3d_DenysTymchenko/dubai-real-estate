import { FC } from 'react';
import './index.scss'
import useExperienceStore from '../../store/useExperienceStore';

interface HintProps {
  showMap: boolean;
}

const Hint: FC<HintProps> = ({ showMap }) => {
  const showHint = useExperienceStore(state => state.showHint)

  return (
    <div className={'hint not-selectable' + (showHint ? ' show' : '')}>
      <p>{showMap ? 'MOVE AROUND' : 'ROTATE'}</p>
      <img
        className={'icon default ' + (showMap ? 'map-scene' : 'build-scene')}
        src='./icons/mouse.svg'
      />
      <img
        className={'icon mobile ' + (showMap ? 'map-scene' : 'build-scene')}
        src='./icons/touch.svg'
      />
    </div>);
};

export default Hint;
