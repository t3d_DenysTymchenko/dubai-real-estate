import { FC } from 'react';
import { Building, buildings } from '../../../../utils/buildings';
import './index.scss'

type BuildingInfoPopUpProps = {
  buildingName: string;
  togglePopUp: () => void;
  openBuildingScene: Function;
}

const BuildingInfoPopUp: FC<BuildingInfoPopUpProps> = ({ buildingName, togglePopUp, openBuildingScene }) => {
  const { info, popup_logo_size } = buildings[buildingName] as Building;
  return (
    <>
      <div className='top-part'>
        <img src={`./logos/${buildingName}.svg`} alt='Building logo' width={popup_logo_size.width} height={popup_logo_size.height} />
        <img src='./icons/close.svg' className='close' onClick={togglePopUp} alt='Close icon' />
      </div>
      <p>Location: {info.location}</p>
      <p>Developer: {info.developer}</p>
      <p>Handover: {info.handover}</p>
      <p>Starting price: <span className={info.starting_price === 'SOLD OUT' ? 'red' : ''}>{info.starting_price}</span></p>
      <p>Unit Size: {info.unit_size}</p>
      <button className='blue' onClick={() => openBuildingScene(buildingName)}>
        Discover
      </button>
    </>
  )
}

export default BuildingInfoPopUp;
