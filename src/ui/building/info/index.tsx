import { FC } from 'react';
import useBuildStore from '../../../store/useBuildStore';
import { Building, buildings } from '../../../utils/buildings';
import './index.scss';

interface BuildingInfoProps {
  toggleInfo: boolean;
  setToggleInfo: React.Dispatch<React.SetStateAction<boolean>>;
}

const BuildingInfo: FC<BuildingInfoProps> = ({ toggleInfo, setToggleInfo }) => {
  const selectedBuilding = useBuildStore((state) => state.selectedBuilding);
  const { info, info_logo_size } = buildings[selectedBuilding] as Building;

  return (
    <div className={'building-info' + (toggleInfo ? ' opened' : '')}>
      <button className='toggle' onClick={() => setToggleInfo(!toggleInfo)}>
        <img className='default' src='./icons/arrow.svg' style={{ rotate: toggleInfo ? '180deg' : '0deg' }} />
        <img className='mobile' src='./icons/arrow-down.svg' />
      </button>

      <div className='info'>
        <img src={`./logos/${selectedBuilding}.svg`} width={info_logo_size.width} height={info_logo_size.height} />

        <div>
          {info.brand && <p><strong>Brand:</strong>&nbsp;{info.brand}</p>}
          <p><strong>Developer:</strong>&nbsp;{info.developer}</p>
        </div>

        <div className='description'>
          <p><strong>Location:</strong>&nbsp;{info.location}</p>
          <p><strong>Structure:</strong>&nbsp;{info.structure}</p>
          <p><strong>Handover:</strong>&nbsp;{info.handover}</p>
          <p><strong>Unit Size:</strong>&nbsp;{info.unit_size}</p>
          <p><strong>Starting price:</strong>&nbsp;<span className={info.starting_price === 'SOLD OUT' ? 'red' : ''}>{info.starting_price}</span></p>
          <p><strong>Total units:</strong>&nbsp;{info.total_units}</p>
          {info.payment_plan && <div>
            <strong>Payment plan:</strong>&nbsp;
            {typeof info.payment_plan === 'string' ? info.payment_plan : (
              <>
                <p style={{marginLeft: '20px', marginTop: '3px', marginBottom: '5px'}}><strong>Post Handover:</strong>&nbsp;{info.payment_plan.post_handover}</p>
                <p style={{marginLeft: '20px'}}><strong>Non-Post Handover:</strong>&nbsp;{info.payment_plan.non_post_handover}</p>
              </>
            )}
          </div>}
        </div>

        <div>
          <h3>Unit mix</h3>
          <p>{info.unit_mix}</p>
        </div>
        <div>
          <h3>Notable features</h3>
          <ul>
            {info.notable_features.map((feature, index) => <li key={index}>{feature}</li>)}
          </ul>
        </div>

        <button className='blue'>Request a Call Now</button>
      </div>
    </div>
  )
}

export default BuildingInfo;
