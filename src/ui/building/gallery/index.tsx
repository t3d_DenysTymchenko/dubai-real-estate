import { FC, useEffect, useState } from 'react';
import GallerySwitch from './switch';
//import useBuildStore from '../../store/useBuildStore';
import galleryImages, { ImagesType } from '../../../utils/galleryImages';
import './index.scss';
import useBuildStore from '../../../store/useBuildStore';

interface BuildingGalleryProps {
  setShowGallery: React.Dispatch<React.SetStateAction<boolean>>;
}

const BuildingGallery: FC<BuildingGalleryProps> = ({ setShowGallery }) => {
  const selectedBuilding = useBuildStore((state) => state.selectedBuilding);
  const images = galleryImages[selectedBuilding];

  const [imagesType, setImagesType] = useState(ImagesType.EXTERIOR);
  const [selectedImage, setSelectedImage] = useState(
    Math.round(
      (images[imagesType].length - 1) / 2
    )
  );

  useEffect(() => {
    if (selectedImage > images[imagesType].length - 1) setSelectedImage(images[imagesType].length - 1)
  }, [imagesType])

  // Formula of correct translateX: [left corner px value] - ([distance between images in px] * [selected image index] )
  const CalculateCorrectTranslateX: React.CSSProperties = {
    transform: `translateX(
      calc(
        (var(--left-corner-pos) -
        (var(--distance-between-images) * ${selectedImage}))
      )
    )`,
  };
  const progressBarPosition: React.CSSProperties = {
    left: `${(selectedImage / (images[imagesType].length - 1)) * 100}%`,
  }

  const goRight = () => {
    if (selectedImage === images[imagesType].length - 1) return;
    const nextImageIndex = selectedImage + 1;
    setSelectedImage(nextImageIndex)
  }

  const goLeft = () => {
    if (selectedImage === 0) return;
    setSelectedImage(selectedImage - 1);
  }

  return (
    <div className='building-gallery'>
      <button className='close' onClick={() => setShowGallery(false)}>
        close
        <img src='./icons/close.svg' alt='Close icon' />
      </button>
      <GallerySwitch onChange={() => setImagesType(imagesType === ImagesType.EXTERIOR ? ImagesType.INTERIOR : ImagesType.EXTERIOR)} />
      {/* <div className='center'></div> */}
      <div className='images-container'>
        <div className='wrapper' style={CalculateCorrectTranslateX}>
          {images[imagesType].map((src, index) => (
            <img src={src} key={index} className={selectedImage === index ? 'selected' : ''} />
          ))}
        </div>
      </div>
      <div className='arrows'>
        <img src='./icons/gallery_arrow.svg' onClick={() => goLeft()} />
        <img src='./icons/gallery_arrow.svg' style={{ rotate: '180deg' }} onClick={() => goRight()} />
      </div>
      <div className='progress'>
        <div className='bar' style={progressBarPosition}></div>
      </div>
    </div>
  );
};

export default BuildingGallery;
