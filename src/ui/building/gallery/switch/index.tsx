import { FC } from 'react';
import './index.scss'

interface GallerySwitchProps {
  onChange: () => void
}
const GallerySwitch: FC<GallerySwitchProps> = ({ onChange }) => {
  return (
    <label className="gallery-switch">
      <input type="checkbox" onChange={onChange} />
      <span className="slider round"></span>
      <div className='text not-selectable'>
        <p>Exterior</p>
        <p>Interior</p>
      </div>
    </label>
  )
}

export default GallerySwitch;
