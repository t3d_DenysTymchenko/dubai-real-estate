import { FC } from 'react';
import './index.scss';
import useBuildStore from '../../../store/useBuildStore';
import { Building, buildings } from '../../../utils/buildings';

interface BuildingPanelProps {
  changeScene: () => void;
  setShowGallery: React.Dispatch<React.SetStateAction<boolean>>;
  setToggleInfo: React.Dispatch<React.SetStateAction<boolean>>;
};

const BuildingPanel: FC<BuildingPanelProps> = ({ changeScene, setShowGallery, setToggleInfo }) => {
  const selectedBuilding = useBuildStore((state) => state.selectedBuilding);
  const { google_maps } = buildings[selectedBuilding] as Building;

  return (
    <>
      <button className='default project-details' onClick={() => setToggleInfo(true)}>
        SEE PROJECT DETAILS
      </button>
      <div className='building-panel not-selectable'>
        <button className='default' onClick={changeScene}>
          <img src='./icons/arrow.svg' />
          <span>Return to map</span>
        </button>

        <div>
          <button className='default'>
            <span>Download Brochure</span>
            <img src='./icons/download.svg' />
          </button>
          <button className='default' onClick={() => setShowGallery(true)}>
            <span>Gallery</span>
            <img src='./icons/gallery.svg' />
          </button>
          <button className='default'>
            <a href={google_maps} target='_blank'>
              <span>Google maps</span>
              <img src='./icons/map.svg' />
            </a>
          </button>
        </div>
      </div>
    </>
  );
}

export default BuildingPanel;
