import { useState } from 'react';
import useBuildStore from '../../../store/useBuildStore';
import { Building, buildings } from '../../../utils/buildings';
import './index.scss';
import useExperienceStore from '../../../store/useExperienceStore';

const BuildingPreview = () => {
  const selectedBuilding = useBuildStore((state) => state.selectedBuilding);
  const toggleShowHint = useExperienceStore((state) => state.toggleShowHint);

  const homeData = buildings[selectedBuilding] as Building;
  const previewImage = `${window.location.origin}/previews/${selectedBuilding}.jpg`;
  const [close, setClose] = useState(false);

  return (
    <>
      <div className={'building-preview' + (close ? ' hide' : '') + (homeData.not_invert ? ' not-invert' : '')} style={{ backgroundImage: `url(${previewImage})` }}>
      </div>
      {!homeData.no_model && !close && (
        <button className='default view-in-3d' onClick={() => {
          setClose(true)
          toggleShowHint()
        }}>
          <Cube3D />
          View in 3D
        </button>
      )}
    </>
  );
};

const Cube3D = () => {
  return (
    <div className="cube-wrap">
      <div className="cube">
        <div className="side top"></div>
        <div className="side bottom"></div>
        <div className="side front"></div>
        <div className="side back"></div>
        <div className="side left"></div>
        <div className="side right"></div>
      </div>
    </div>
  )
}

export default BuildingPreview;
