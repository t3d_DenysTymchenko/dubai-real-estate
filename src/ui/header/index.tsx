import Select from '../Select';
import './index.scss'

const Header = () => {
  return (
    <header className="not-selectable">
      <div className="logo">EVOLUTIONS</div>
      <div style={{ position: 'absolute', paddingLeft: '220px', width: '100%' }}>
        <Select />
      </div>
      <button>Reach out</button>
    </header>
  );
};

export default Header;
